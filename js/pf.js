var serviceURL = 'https://wildfly-persofeedback.rhcloud.com/personalized-feedback/feedback/pf/match';
jQuery.support.cors = true;

// Wildfly idles if it is not accessed in 24 hours, we aim to wake up the server when the homepage is loaded to avoid errors.
function warmUpServer() {
	$.ajax({
        url: serviceURL,
        "data": { },
        success: function (response) { },
        error: function(xhr, error){ }
    });
}

function match(button) {
	var $button = $(button);
	$button.button('loading');
	
	displayEPDG('[]');
	displayFeedback();
	
	setTimeout(function() {
		$.ajax({
	        url: serviceURL,
	        "data": {
	        	assignment: 1,
	        	submission: ace.edit('editor').getValue(),
	        	patterns: 'ACCESS_EVEN--' + document.getElementById('patternA-select').value + ',' + 
	        				'ACCESS_ODD--' + document.getElementById('patternB-select').value + ',' +
	        				'CUMULATIVELY_ADD--' + document.getElementById('patternC-select').value + ',' +
	        				'CUMULATIVELY_MULT--' + document.getElementById('patternD-select').value + ',' +
	        				'PRINT_CONSOLE--' + document.getElementById('patternE-select').value
	        },
	        success: function (response) {
	        	var obj = JSON.parse(response);
	        	if (obj.graph) {
	        		displayEPDG(obj.graph);
	        		displayFeedback(obj.feedback);
	        	} else {
	        		var descErr = document.getElementById('description-error');
	        		descErr.innerHTML = '';
	        		$.each(obj.error, function(i, item) {
	        			descErr.innerHTML += item + '<br />';
	        		});
	        		$('#error').modal('show');
	        	}
	        	$button.button('reset');
	        },
	        error: function(xhr, error){
	        	console.log(xhr);
	        	console.log(error);
	        	
	        	var descErr = document.getElementById('description-error');
	    		descErr.innerHTML = 'Something went wrong when connecting to the server.';
	    		$('#error').modal('show');
	    		$button.button('reset');
	        }
	    });
	}, 1000);
}

function getImageTag(type) {
	var ret = '<img width="30px" src="img/';
	if (type == 'Correct')
		ret += 'up.png" alt="Correct" ';
	if (type == 'Incorrect')
		ret += 'down.png" alt="Incorrect" ';
	if (type == 'Almost')
		ret += 'almost.png" alt="Almost" ';
	ret += '>';
	return ret;
}

function displayFeedback(feedbackData) {
	var list = document.getElementById('feedback');
	list.innerHTML = "";
	var i = 0;
	
	$.each(feedbackData, function(i, item) {
		var heading = document.createElement("div");
		heading.className = 'panel-heading collapsed';
		
		var headingHTML ='<h4 class="panel-title">' + getImageTag(item.type) + '&nbsp;' + item.text + 
			'&nbsp;<a data-toggle="collapse" data-parent="#feedback-group" href="#feed' + i + '""><img alt="More" src="img/more.png" width="20px" /></a></h4>';
		
		heading.innerHTML = headingHTML;
		
		var feedbackDiv = document.createElement("div");
		feedbackDiv.id = 'feed' + i;
		feedbackDiv.className = 'panel-collapse collapse';
		
		var minorList = document.createElement("ul");
		minorList.className = 'list-group';
		
		$.each(item.minor, function(j, minor) {
			var minorElement = document.createElement("li");
			
			minorElement.className = 'list-group-item';
			minorElement.innerHTML = '<table><tr><td style="padding: 15px;">' 
				+  getImageTag(minor.type) + '</td><td>' + minor.text + '</td></tr></table>';
			
			minorList.appendChild(minorElement);
		});
		feedbackDiv.appendChild(minorList);
		
		list.appendChild(heading);
		list.appendChild(feedbackDiv);
	});
}

function displayEPDG(graphData) {
	var pdg = cytoscape({
		  container: document.getElementById('pdg') // container to render in
	});
	pdg.elements().removeData('elements');
	pdg.json({
		boxSelectionEnabled: false,
		autounselectify: true,
		zoom: 0,
		minZoom: 0.5,
		maxZoom: 5, 

  
		style: [
		{
			selector: 'node',
		    style: {
		    	'shape': 'roundrectangle',
		    	'width': 'label',
		        'content': 'data(label)',
		        'text-valign': 'center',
		        'background-color': 'white',
		        'border-width': 1,
		        'border-color': 'blue',
		        'padding-left': 3,
		        'padding-right': 3
		    }
		  },
		  {
				selector: 'edge',
			    style: {
					'width': 2,
					'target-arrow-shape': 'triangle',
					'opacity': 0.666,
					'line-color': 'blue',
					'curve-style': 'bezier',
					'source-arrow-color': 'blue',
			        'target-arrow-color': 'blue'
			    }
		  },
		  {
			selector: 'edge.control',
		    style: {
				'line-style': 'dashed'
		    }
		  }
		],
		elements: graphData
	});
	pdg.layout({
		name: 'grid'
	});
	pdg.on('mouseover', function(evt){
		if (findFunction(evt.cyTarget, 'id'))
			mouseOverNodes('code-nodes', evt);
	});
	loadNodes(pdg, 'code-nodes', 'v');
}

function findFunction(obj, name) {
	var found = false;
	for (var k in obj) {
		if (k == name)
			found = true;
	}
	return found;
}

function mouseOverNodes(id, evt) {
	$(id).scrollTop(100);
	var listOfNodes = document.getElementById(id).getElementsByTagName('li');
	
	for (var i = 0; i < listOfNodes.length; i++) {
		var item = listOfNodes[i];
		
		item.className = 'list-group-item code-node-item';
		item.tabIndex = i;
		item.style = '';
		if (item.value == evt.cyTarget.id()) {
			item.className += ' active';
			item.focus();
		}
	}
}

function loadNodes(pdg, id, nodePrefix) {
	var list = document.getElementById(id);
	list.innerHTML = "";
	
	pdg.nodes('[code]').forEach(function (node, i, all) {
		var li = document.createElement("li");
		li.className = 'list-group-item code-node-item';
		li.value = node.id();
		li.appendChild(document.createTextNode(nodePrefix + node.id() + ': ' + node.data('code')));
		list.appendChild(li);
	});
}

function changeSubmission(selected) {
	var editor = ace.edit("editor");
	var text = "/* Student's code here! */\n";
	var errorText;
	
	if (selected.value == 1) {
		text = "int odd = 0, even = 0;\n" +
		"for (int i = 0; i <= a.length; i++) {\n" +
		"\tif (i % 2 == 1)\n" +
		"\t\todd += a[i];\n" +
		"\tif (i % 2 == 1)\n" +
		"\t\teven *= a[i];\n" +
		"}\n" +
		"System.out.println(\"Odd: \" + odd);";
		
		errorText = 'It is accessing odd positions twice and variable <i>even</i> is not printed to console.';
	}
	
	if (selected.value == 2) {
		text = "int o = 0, e = 1, i = 0;\n" +
		"while (i < a.length) {\n" +
		"\tif (i % 2 == 1)\n" +
		"\t\to += a[i];\n" +
		"\tif (i % 2 == 0)\n" +
		"\t\te *= a[i];\n" +
		"\ti++;\n" +
		"}\n" +
		"System.out.println(o + \", \" + e);";
		
		errorText = 'Correct submission.<br/>';
	}
	
	if (selected.value == 3) {
		text = "int x = 0;\n" +
		"for (int i = 0; i < a.length; i++)\n" +
		"\tif (i % 2 == 1)\tx *= a[i];\n" +	
		"int y = 1;\n" +
		"for (int i = 0; i < a.length; i++)\n" +
		"\tif (i % 2 == 0)\ty += a[i];\n" +
		"System.out.println(\"Odd: \" + x + \", Even: \" + y);";
		
		errorText = 'Accumulated variables start in 0 and 1 when it should be the other way around.';
	}
	
	if (selected.value == 4) {
		text = "int odd = 0, even = 0, i = 0;\n" +
		"while (i < a.length) {\n" +
		"\todd *= a[i];\n" +
		"\teven += a[i];\n" +
		"}\n" +
		"System.out.println(odd + \",\" + even);";
		
		errorText = 'Multiple errors.<br/>';
	}
	
	if (selected.value == 5) {
		text = "int o = 1, e = 0, x = 1;\n" +
		"while (x <= a.length) {\n" +
		"\tif (x % 2 == 1)\to *= a[x];\n" +
		"\tif (x % 2 == 0)\te += a[x];\n" +
		"\tx++;\n" +
		"}\n" +
		"System.out.println(o + \", \" + e);";
		
		errorText = 'Loop index starts in 1 and goes beyond <i>a.length</i>.';
	}
	
	if (selected.value == 6) {
		text = "int o = 1, e = 0;\n" +
		"for (int i = 0; i < a.length; i++) {\n" +
		"\tif (i % 2 == 1) \n" +
		"\t\to *= a[i - 1];\n" +
		"\tif (i % 2 == 0) \n" +
		"\t\te += a[i];\n" +
		"}\n" +
		"System.out.println(e + \", \" + o);";
		
		errorText = 'Array is accessed using <i>i - 1</i>.<br/>';
	}
	
	if (selected.value == 7) {
		text = "int o = 1, e = 0, x = 0;\n" +
		"while (x < a.length) {\n" +
		"\tif (x % 2 == 1)\to += a[x];\n" +
		"\tif (x % 2 == 0)\te *= a[x];\n" +
		"\tx++;\n" +
		"}\n" +
		"System.out.println(o);\n" +
		"System.out.println(e);";
		
		errorText = 'Accumulated variables start in 0 and 1 when it should be the other way around.';
	}
	
	if (selected.value == 8) {
		text = "int o = 1, e = 0, i = 0;\n" +
		"while (i < a.length); {\n" +
		"\tif (i % 2 == 1)\to *= a[i];\n" +
		"\tif (i % 2 == 0)\te += a[i];\n" +
		"\ti++;\n" +
		"}\n" +
		"System.out.println(\"Even: \" + e + \", Odd: \" + o);";
		
		errorText = 'Multiple errors (it is incorrect due to the semicolon at the end of the while loop).';
	}
	
	if (selected.value == 9) {
		text = "int even = 0, i = 0;\n" +
		"for (; i < a.length; i++)\n" +
		"\tif (i % 2 == 0)\teven += a[i];\n" +
		"int odd = 1;\n" +
		"for (; i < a.length; i++)\n" +
		"\tif (i % 2 == 1)\todd *= a[i];\n" +
		"System.out.println(\"Even: \" + even + \", Odd: \" + odd);";
		
		errorText = 'Variable <i>i</i> is not initialized again in the second loop.';
	}
	
	if (selected.value == 10) {
		text = "int e = 0;\n" +
		"for (int i = 0; i < a.length; i+=2)\n" +
		"\te += a[i];\n" +
		"int o = 1;\n" +
		"for (int i = 1; i < a.length; i+=2)\n" +
		"\to *= a[i];\n" +
		"System.out.println(o);\n" +
		"System.out.println(e);";
		
		errorText = 'Correct but our patterns do not detect it.';
	}
	
	document.getElementById('error-description').innerHTML = errorText;
	editor.getSession().setValue(text);
}

function displayPattern(patternName) {
	document.getElementById(patternName).style.zIndex = '1000';


    var pdgPattern = cytoscape({
		  container: document.getElementById(patternName) // container to render in
	});
    pdgPattern.elements().removeData('elements');
    pdgPattern.json({
		boxSelectionEnabled: false,
		autounselectify: true,
		zoom: 0,
		minZoom: 0.5,
		maxZoom: 5,
  
		style: [
		{
			selector: 'node',
		    style: {
		    	'shape': 'roundrectangle',
		    	'width': 'label',
		        'content': 'data(label)',
		        'text-valign': 'center',
		        'background-color': 'white',
		        'border-width': 1,
		        'border-color': 'blue',
		        'padding-left': 3,
		        'padding-right': 3
		    }
		  },
		  {
				selector: 'edge',
			    style: {
					'width': 2,
					'target-arrow-shape': 'triangle',
					'opacity': 0.666,
					'line-color': 'blue',
					'curve-style': 'bezier',
					'source-arrow-color': 'blue',
			        'target-arrow-color': 'blue'
			    }
		  },
		  {
			selector: 'edge.control',
		    style: {
				'line-style': 'dashed'
		    }
		  }
		],
        elements: getElement(patternName)
    });
    pdgPattern.layout({
        name: 'concentric'
    });
    pdgPattern.on('mouseover', function(evt){
        if (findFunction(evt.cyTarget, 'id'))
        	mouseOverNodes(patternName + '-nodes', evt);
    });
    loadNodes(pdgPattern, patternName + '-nodes', 'u');
}

function getElement(patternName) {
	var jSon;
    if (patternName == 'pdgA')
        jSon = '[{"data": { "id": 1, "label": "u1, decl", "code": "s" }},'+
				'{"data": { "id": 2, "label": "u2, assign", "code": "x = 0" }},'+
				'{"data": { "id": 3, "label": "u3, assign", "code": "x++" }},'+
				'{"data": { "id": 4, "label": "u4, ctrl", "code": "x < s.length" }},'+
				'{"data": { "id": 5, "label": "u5, ctrl", "code": "x%2 == 0" }},'+
				'{"data": { "id": 6, "label": "u6,", "code": "_s[x]_" }},'+
				'{"data": { "id": "1-->4", "source": 1, "target": 4 }},'+
				'{"data": { "id": "1-->6", "source": 1, "target": 6 }},'+
				'{"data": { "id": "2-->4", "source": 2, "target": 4 }},'+
				'{"data": { "id": "2-->3", "source": 2, "target": 3 }},'+
				'{"data": { "id": "2-->6", "source": 2, "target": 6 }},'+
				'{"data": { "id": "2-->5", "source": 2, "target": 5 }},'+
				'{"data": { "id": "4-->4", "source": 4, "target": 4 }, "classes": "control"},'+
				'{"data": { "id": "4-->3", "source": 4, "target": 3 }, "classes": "control"},'+
				'{"data": { "id": "4-->5", "source": 4, "target": 5 }, "classes": "control"},'+
				'{"data": { "id": "5-->6", "source": 5, "target": 6 }, "classes": "control"}]';
    else if (patternName == 'pdgB')
        jSon = '[{"data": { "id": 1, "label": "u1, decl", "code": "s" }},'+
		        '{"data": { "id": 2, "label": "u2, assign", "code": "x = 0" }},'+
		        '{"data": { "id": 3, "label": "u3, assign", "code": "x++" }},'+
		        '{"data": { "id": 4, "label": "u4, ctrl", "code": "x < s.length" }},'+
		        '{"data": { "id": 5, "label": "u5, ctrl", "code": "x%2 == 1" }},'+
		        '{"data": { "id": 6, "label": "u6,", "code": "_s[x]_" }},'+
		        '{"data": { "id": "1-->4", "source": 1, "target": 4 }},'+
		        '{"data": { "id": "1-->6", "source": 1, "target": 6 }},'+
		        '{"data": { "id": "2-->4", "source": 2, "target": 4 }},'+
		        '{"data": { "id": "2-->3", "source": 2, "target": 3 }},'+
		        '{"data": { "id": "2-->6", "source": 2, "target": 6 }},'+
		        '{"data": { "id": "2-->5", "source": 2, "target": 5 }},'+
		        '{"data": { "id": "4-->4", "source": 4, "target": 4 }, "classes": "control"},'+
		        '{"data": { "id": "4-->3", "source": 4, "target": 3 }, "classes": "control"},'+
		        '{"data": { "id": "4-->5", "source": 4, "target": 5 }, "classes": "control"},'+
		        '{"data": { "id": "5-->6", "source": 5, "target": 6 }, "classes": "control"}]';
    else if (patternName == 'pdgC')
        jSon = '[{"data": { "id": 1, "label": "u1, assign", "code": "c = 0" }},'+
        		'{"data": { "id": 2, "label": "u2, assign", "code": "c += _" }},'+
        		'{"data": { "id": 3, "label": "u3, ctrl", "code": "_" }},'+
        		'{"data": { "id": 4, "label": "u4, ctrl", "code": "_" }},'+
        		'{"data": { "id": "1-->2", "source": 1, "target": 2 }},'+
        		'{"data": { "id": "3-->2", "source": 3, "target": 2 }, "classes": "control"},'+
        		'{"data": { "id": "4-->3", "source": 4, "target": 3 },"classes": "control"},'+
        		'{"data": { "id": "4-->4", "source": 4, "target": 4 }, "classes": "control"}]';
    else if(patternName == 'pdgD')
        jSon = '[{"data": { "id": 1, "label": "u1, assign", "code": "c = 0" }},'+
				'{"data": { "id": 2, "label": "u2, assign", "code": "c *= _" }},'+
				'{"data": { "id": 3, "label": "u3, ctrl", "code": "_" }},'+
				'{"data": { "id": 4, "label": "u4, ctrl", "code": "_" }},'+
				'{"data": { "id": "1-->2", "source": 1, "target": 2 }},'+
				'{"data": { "id": "3-->2", "source": 3, "target": 2 }, "classes": "control"},'+
				'{"data": { "id": "4-->3", "source": 4, "target": 3 },"classes": "control"},'+
				'{"data": { "id": "4-->4", "source": 4, "target": 4 }, "classes": "control"}]';
    else if(patternName == 'pdgE')
        jSon ='[{"data": { "id": 1, "label": "u1, assign", "code": "x = _" }},'+
        		'{"data": { "id": 2, "label": "u2, call", "code": "System.out.print(x)"}},'+
        		'{"data": { "id": "1-->2", "source": 1, "target": 2 }}]';
    
    return JSON.parse(jSon);
}